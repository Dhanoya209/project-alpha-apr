from django.db import models
from django.contrib.auth.models import User


# Create your models here.
# create Project model
class Project(models.Model):
    # name attribute charfield max 200
    name = models.CharField(max_length=200)
    # description string no max length
    description = models.TextField()
    # members many to many refer to auth.user model related name "projects"
    members = models.ManyToManyField(
        User,
        related_name="projects",
    )

    # Convert to string value of name using __str__
    def __str__(self):
        return self.name
